<img width="130" src="https://psv4.userapi.com/c856216/u169946525/docs/d9/a0ee81b5e810/Group_2.png?extra=Xna9XehL1cpeFkuI_PSEzMxhpE3vTmJv9yzOefzLcgskBdj6bB6JOYkrsPu2IjCu4lfdxnjDPszdVYjdbLGWN40CRjO9uVxNnIBR-FjWKgi1rPQJOiQEvnidXpfTnHweN8lVmCJE0GQtUwZIzo1DAZMI">

## Как запустить

### Приложение доступно на сервисе Zeit

`bafomet-mini-app.vercel.app`

### Запуск на своем компьютере

1) Скачайте архив или склонируйте к себе репозиторий
2) Выполните команду `npm install` для установки зависимостей
> Обратите внимание, что у вас должны быть установлены `react-scripts` `cross-env` и `babel/cli`
> Для их установки выполните следующие команды:
> * `npm i @babel/cli`
> * `npm i cross-env`
> * `npm i react-scripts`
>
> Рекомендуем выпополнить установку глобально

3) Выполните команду `npm start` для запуска приложения

[Ссылка на макеты Figma](https://www.figma.com/file/lLN1sBDrUjPZ9yuE0SUtnl/%D0%92%D0%B8%D0%B7%D0%B8%D1%82%D0%BD%D0%B8%D1%86%D0%B0?node-id=49%3A1)