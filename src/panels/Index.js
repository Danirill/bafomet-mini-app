import React, { Component } from 'react';
import PropTypes, { element } from 'prop-types';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import css from '../css/style.css';
import FixedLayout from '@vkontakte/vkui/dist/components/FixedLayout/FixedLayout';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import Epic from '@vkontakte/vkui/dist/components/Epic/Epic';
import {Tabbar, Search, Tabs, TabsItem, Root, Text, Button, Checkbox, Link, FormLayout, PanelHeaderButton, SimpleCell, Avatar, Group, Header,
    Select, Input, Radio,Textarea, FormLayoutGroup

} from '@vkontakte/vkui';
import Icon16Dropdown from '@vkontakte/icons/dist/16/dropdown';
import Icon28UserCircleOutline from '@vkontakte/icons/dist/28/user_circle_outline';
import Icon24Filter from '@vkontakte/icons/dist/24/filter';
import Icon28ScanViewfinderOutline from '@vkontakte/icons/dist/28/scan_viewfinder_outline';
import TabbarItem from '@vkontakte/vkui/dist/components/TabbarItem/TabbarItem';
import View from '@vkontakte/vkui/dist/components/View/View';
import Card from '@vkontakte/vkui/dist/components/Card/Card';
import CardScroll from '@vkontakte/vkui/dist/components/CardScroll/CardScroll';
import Icon28SortOutline from '@vkontakte/icons/dist/28/sort_outline';
import Icon28MailOutline from '@vkontakte/icons/dist/28/mail_outline';
import Icon28PaymentCardOutline from '@vkontakte/icons/dist/28/payment_card_outline';
import Icon28Favorite from '@vkontakte/icons/dist/28/favorite';
import Icon28ShareOutline from '@vkontakte/icons/dist/28/share_outline';
import Icon24Back from '@vkontakte/icons/dist/24/back';
import {
    EmailShareButton,
    FacebookShareButton,
    InstapaperShareButton,
    LineShareButton,
    LinkedinShareButton,
    LivejournalShareButton,
    MailruShareButton,
    OKShareButton,
    PinterestShareButton,
    PocketShareButton,
    RedditShareButton,
    TelegramShareButton,
    TumblrShareButton,
    TwitterShareButton,
    ViberShareButton,
    VKShareButton,
    WhatsappShareButton,
    WorkplaceShareButton
  } from "react-share";

class Index extends React.Component {
    constructor (props) {
      super(props);
      this.memory = props.memory;
      this.filters = props.mFilters;
      this.isDataloaded = false;
      this.renderedCards = [];
      this.checkboxes = [];
      this.state = {
        activePanel:'my_visits',
        activeTab:'liked',
        isFilterOpen: false,
        cardStates: "cardVisit",
        searchText:"",
        phone:"",
        description:"",
        tag:'',
        user:''
      };
      this.cards_array = []; 
      this.onPanelChange = this.onPanelChange.bind(this);
      this.loadCards = this.loadCards.bind(this);
      this.renderCards = this.renderCards.bind(this);
      this.reRender = this.reRender.bind(this);
      this.createCheckboxes = this.createCheckboxes.bind(this);
      this.onChange = this.onChange.bind(this);
      this.handleCheckBoxChange = this.handleCheckBoxChange.bind(this);
      this.handleChangeDes = this.handleChangeDes.bind(this);
      this.handleChangeUser = this.handleChangeUser.bind(this);
      this.handleChangePhone = this.handleChangePhone.bind(this);
      this.handleChangeTag = this.handleChangeTag.bind(this);
      this.addCard = this.addCard.bind(this);

    }

    addCard(){      
        this.memory.mCard_data.push({
            mCutaway_id:'1',
            mCreater_id: this.state.user,
            mCard_tag:'',
            mCities:[1],
            mTags:[
                1,2
            ],
            mIsCountryCompany: false,
            mVk_group_id:'123',
            mName:'Фотограф',
            mDescription:this.state.description,
            mInstagram_link:'',
            mTelegram_link:'',
            mViber_link:'',
            mWhatsapp_link:'',
            mWebsite_link:'',
            mBackColor: '#A4AEE9',
            mPhone: this.state.phone,
            mLikes: "В избранном у 153 людей"
        });
        this.setState({activePanel:'lk'});
    }

    handleChangeDes(event) {
        this.setState({description: event.target.value});
    }

    handleChangePhone(event) {
        this.setState({phone: event.target.value});
    }

    handleChangeUser(event) {
        this.setState({user: event.target.value});
    }

    handleChangeTag(event) {
        this.setState({tag: event.target.value});
    }
          

    handleCheckBoxChange(event) {
        let target = event.target;
        console.log(JSON.stringify(event.target.value));
        for(let i=0; i< this.memory.mFilters.mTags.length;i++){
            if(target.value == this.memory.mFilters.mTags[i].mTag_id){
                let currentChecked = this.memory.mFilters.mTags[i].checked;
                this.memory.mFilters.mTags[i].checked = !currentChecked;
            }     
        }
      }   
  
    createCheckboxes(){
        this.checkboxes = [];
        this.memory.mFilters.mTags.forEach(element => {
            this.checkboxes.push(
            <Checkbox defaultChecked={element.checked} value={element.mTag_id} onChange={this.handleCheckBoxChange}>{element.mTag_name}</Checkbox>
            );
        })       
    }

    onPanelChange (panel_name) {
      this.setState({ activePanel: panel_name })
    }

    loadCards(){
        this.cards_array = []; 
        this.cards_array = this.memory.mCard_data.filter(element => {
            let city_flag = false;
            let tag_flag = false;
            let vk_group_flag = false;
            let searchTextflag = false;

            console.log(JSON.stringify(element));
            console.log(JSON.stringify(this.memory.mFilters));

            element.mCities.forEach(city_id => {
                if(this.memory.mFilters.mCities.filter(x => x.mCity_id == city_id).length > 0){
                    city_flag = true;
                }                    
            })

            if(!city_flag)
                return false;
            

            /*element.mTags.forEach(tag_id => {
                if(this.memory.mFilters.mTags.filter(x => x.mTag_id === tag_id).length > 0){
                    tag_flag = true;
                }                    
            })*/

            this.memory.mFilters.mTags.forEach(tag => {
                if(tag.checked){
                    if(element.mTags.filter(x => x === tag.mTag_id).length > 0){
                        tag_flag = true;
                    }
                }
            })

            if(!tag_flag)
                return false;

            vk_group_flag = element.vk_group_flag == null ? true : false === this.memory.mFilters.isVkGroup;

            if(element.mDescription.toLowerCase().indexOf(this.state.searchText.toLowerCase()) > -1){
                searchTextflag = true;
            }
            
            return city_flag && tag_flag && vk_group_flag && searchTextflag;   
        })
    }

    reRender(element){
        if(this.state.cardStates == 'cardVisit'){
            this.setState({cardStates: 'cardVisit cardVisitFull'});
        }          
        else{
            this.setState({cardStates: 'cardVisit'});
        }    
    }

    renderCards(){
        console.log("renderCards");
        this.renderedCards = [];
        this.cards_array.forEach(element =>{
            this.renderedCards.push(
            <div class = {this.state.cardStates} style={{backgroundColor: element.mBackColor}}>
            <div style = {{ float: "right", display:"flex", flexDirection:"column"}} >
            <Text weight="regular" style={{fontSize: "10px"}}>{element.mLikes}</Text>
                <Icon28Favorite style={{marginLeft:"100px"}}/>
                <VKShareButton title="Визитка вк" url="https://vk.com"><Icon28ShareOutline style={{marginLeft:"100px"}}/></VKShareButton>      
            </div>
            <Text weight="semibold" style={{fontSize: "20px", margin: "10px"}}>{element.mCreater_id}</Text>
            <Text style={{fontSize: "12px", margin: "10px"}}>{element.mName}</Text>
            <Text style={{fontSize: "12px", margin: "10px"}}>{element.mPhone}</Text>
            <div style={{display:"flex", flexDirection:"row", marginTop: "50px"}}>
                <Button>Сообщество</Button> 
                <Icon28MailOutline style={{marginLeft:"10px"}}/> 
                <Icon28PaymentCardOutline  style={{marginLeft:"10px"}}/>
                <Text weight="regular" style={{fontSize: "12px", float:"right", marginLeft:"40px"}} onClick={() => this.reRender(element)}>смотреть больше ⅴ</Text>                             
            </div>
            <div class = "blockFull">
                <Text style={{fontSize: "12px", margin: "10px"}}>Описание</Text>
                <Text style={{fontSize: "10px", margin: "10px"}}>{element.mDescription}</Text>
                <Text style={{fontSize: "12px", margin: "10px"}}>Товары</Text>
            </div>
            </div>
            );
        });
    }

    onChange(e) {
        console.log("onChange" + typeof(e))
        this.setState({searchText: e.target.value});
    }

    render () {     
        this.loadCards();
        this.renderCards();
        this.createCheckboxes();
        
      return (   
        <View activePanel={this.state.activePanel}>    
            <Panel id="my_visits">
                <FixedLayout vertical="top">
                        <Search icon={<Icon24Filter/>} onIconClick={() => {this.setState({activePanel: "filters"})}} onChange={this.onChange} after={null}/>                       
                        <Tabs>
                            <TabsItem><Icon28ScanViewfinderOutline/></TabsItem>
                            <TabsItem
                            onClick={() => {
                                this.setState({ activeTab: 'liked'})
                            }}
                            selected={this.state.activeTab === 'liked'}
                            >
                            <span class="textTab">Избранное</span>
                            </TabsItem><TabsItem
                            onClick={() => {
                                this.setState({ activeTab: 'catalog'})
                            }}
                            selected={this.state.activeTab === 'catalog'}
                            >
                             <span class="textTab">Каталог</span>
                            </TabsItem>
                            <TabsItem><Icon28UserCircleOutline onClick={() => {this.setState({activePanel: "lk"})}}/></TabsItem>
                        </Tabs>
                </FixedLayout>         
                <Root activeView={this.state.activeTab}>
                    <View id='liked'>

                        <div style={{borderRadius: "20px 20px 0 0 ", backgroundColor: "#fff", height: "1000px", marginTop: "20px", paddingTop: "1px"}}>
                            <Text weight="medium" style = {{float: "right", display: "flex", flexDirection:"row", marginRight: "10px", alignItems:"center"}}>Недавно добавленные <Icon28SortOutline/> </Text> 
                            <br/>
                            {this.renderedCards}
                            
                        </div>
                    </View>
                    <View id='catalog'>
                    <CardScroll>
                        <Card size="s" style={{backgroundImage: "url(https://i.ibb.co/ZzXJC7D/Rectangle-18.gif)", borderRadius:"10px", backgroundSize:"2", backgroundPosition:"center"}}>
                        <div style={{ width: 80, height: 96 }}>
                            
                        </div>
                        </Card>
                        <Card size="s" style={{backgroundImage: "url(https://i.ibb.co/jMz8GqP/Rectangle-17.gif)", borderRadius:"10px", backgroundSize:"2", backgroundPosition:"center"}}>
                        <div style={{ width: 80, height: 96 }}>

                        </div>
                        </Card>
                        <Card size="s" style={{backgroundImage:"url(https://i.ibb.co/pWsC4Sb/Rectangle-19.gif)", borderRadius:"10px", backgroundSize:"2", backgroundPosition:"center"}}>
                        <div style={{ width: 80, height: 96 }}>
                            
                        </div>
                        </Card>
                        <Card size="s" style={{backgroundImage:"url(https://i.ibb.co/t3DRwc8/Rectangle-21.gif)", borderRadius:"10px", backgroundSize:"2", backgroundPosition:"center"}}>
                        <div style={{ width: 80, height: 96 }}>
                            
                        </div>
                        </Card>
                        <Card size="s" style={{backgroundImage:"url(https://i.ibb.co/m6XxDXv/Rectangle-22.gif)", borderRadius:"10px", backgroundSize:"2", backgroundPosition:"center"}}>
                        <div style={{ width: 80, height: 96 }}>
                           
                        </div>
                        </Card>
                    </CardScroll>
                        <div style={{borderRadius: "20px", backgroundColor: "#fff", height: "1000px", marginTop: "20px", paddingTop:"10px"}}>
                            {this.renderedCards}
                        </div>  
                    </View>
                </Root>       
            </Panel>
            <Panel id="filters">
                <PanelHeader
                    left={<PanelHeaderButton><Icon24Back onClick={() => this.setState({activePanel:"my_visits"})}/></PanelHeaderButton>}
                    >Фильтры
                </PanelHeader>
                <Group header={<Header mode="secondary">Категории</Header>}>
                    <FormLayout>
                        {this.checkboxes}  
                    </FormLayout>
                </Group> 
            </Panel> 
            <Panel id="lk">
            <PanelHeader
            left={<PanelHeaderButton ><Icon24Back onClick={() => this.setState({activePanel:"my_visits"})}/></PanelHeaderButton>}
            >Личный кабинет</PanelHeader>
                <div class="profile">
                   <Avatar size={60} src="https://i.ibb.co/9WD5DhQ/Ellipse8.gif" style={{margin:"0 auto"}}/> 
                   <Text weight="semibold" style={{textAlign:"center", marginTop:"13px"}}>
                       Ксения Виленская
                   </Text>
                   <Text style={{textAlign:"center"}}>
                       Екатеринбург
                   </Text>
                   <div class="myVisit">
                        <Text weight="semibold" style={{marginTop:"18px"}}>
                           Мои визитки
                        <div class="addVisit" onClick={() => this.setState({activePanel:"createVisit"})}>
                         <span>+</span>
                        </div>
                        </Text>
                   </div>
                   {this.renderedCards}
                </div>    
            </Panel>  
            <Panel id="createVisit">
            <PanelHeader
            left={<PanelHeaderButton ><Icon24Back onClick={() => this.setState({activePanel:"lk"})}/></PanelHeaderButton>}
            >Создание визитки</PanelHeader>
                <FormLayout>
            

            <Input id="name_surname" value={this.state.user} onChange={this.handleChangeUser} top="Имя и Фамилия" />
            <Input id="tag" value={this.state.tag} onChange={this.handleChangeTag} top="Деятельность" />
            <Input id="phone" value={this.state.phone} onChange={this.handleChangePhone} top="Номер" />
            <Textarea id="description" value={this.state.description} onChange={this.handleChangeDes} top="Описание" />

            <Checkbox>Согласен с <Link href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">правилами сервиса </Link></Checkbox>
            <Button size="xl" onClick={this.addCard} style={{height:"40px"}}>Создать</Button>
          </FormLayout>
            </Panel>          
        </View>  
      )
    }
  }
  
export default Index;