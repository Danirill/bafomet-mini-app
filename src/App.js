import React, { useState, useEffect } from 'react';
import bridge from '@vkontakte/vk-bridge';
import View from '@vkontakte/vkui/dist/components/View/View';
import ScreenSpinner from '@vkontakte/vkui/dist/components/ScreenSpinner/ScreenSpinner';
import '@vkontakte/vkui/dist/vkui.css';

import Home from './panels/Home';
//import Persik from './css/Persik';
import Index from './panels/Index';

//memory model

let mCities = [
	{
		mCity_id: 1,
		mCity_name:'екб',
		checked: true
	}
]

let mTags = [
	{
		mTag_id:1,
		mTag_name:'Фото',
		checked: true
	},
	{
		mTag_id:2,
		mTag_name:'Еда',
		checked: true
	},
	{
		mTag_id:3,
		mTag_name:'Одежда',
		checked: true
	},
	{
		mTag_id:4,
		mTag_name:'Украшения',
		checked: true
	}
]

let mCard_data = [
	{
		mCutaway_id:'1',
		mCreater_id:'Марк Куклин',
		mCard_tag:'',
		mCities:[1],
		mTags:[
			1
		],
		mIsCountryCompany: false,
		mVk_group_id:'123',
		mName:'Фотограф',
		mDescription:'Фотосъемка студийная и на улице. Прайс и портфолио можно посмотреть в моей группе.',
		mInstagram_link:'',
		mTelegram_link:'',
		mViber_link:'',
		mWhatsapp_link:'',
		mWebsite_link:'',
		mBackColor: '#A4AEE9',
		mPhone:'8-800-555-35-35',
		mLikes: "В избранном у 153 людей"
	},
	{
		mCutaway_id:'1',
		mCreater_id:'Катя Маркова',
		mCard_tag:'',
		mCities:[1],
		mTags:[
			1,4
		],
		mIsCountryCompany: false,
		mVk_group_id:'123',
		mName:'Вышивка',
		mDescription:'Вышивка на футболках',
		mInstagram_link:'',
		mTelegram_link:'',
		mViber_link:'',
		mWhatsapp_link:'',
		mWebsite_link:'',
		mBackColor: '#EE93F0',
		mPhone:'8-800-555-35-35',
		mLikes: "В избранном у 153 людей"
	},
	{
		mCutaway_id:'1',
		mCreater_id:'ART CRAFT',
		mCard_tag:'',
		mCities:[1],
		mTags:[
			1,4
		],
		mIsCountryCompany: false,
		mVk_group_id:'123',
		mName:'Кожа',
		mDescription:'Изделия из кожи',
		mInstagram_link:'',
		mTelegram_link:'',
		mViber_link:'',
		mWhatsapp_link:'',
		mWebsite_link:'',
		mBackColor: '#4C5DF5',
		mPhone:'8-800-555-35-35',
		mLikes: "В избранном у 153 людей"
	}

]

let mUser_data = [
	{
		mVk_user_id:'',
		mLiked_cards:[ //card_id's
			{
				mCard_id:'',
				mAdding_time:'',
				mOpen_counter: null
			}
		],
		mCreated_cards:[ //card_id's
			{
				mCard_id:'',
				mCdding_time:'',
			}
		]
	}
]

let mStories_data = [
	{
		mStories_id:'',
		mText:'',
		mLink:''
	}
]

let mFilters = {
	mCities:mCities,
	mTags:mTags,
	isVkGroup: true
}


let memory = {
	mCard_data: mCard_data,
	mUser_data: mUser_data,
	mStories_data: mStories_data,
	mFilters: mFilters,
	mCities: mCities,
	mTags:mTags
}

const App = () => {
	const [activePanel, setActivePanel] = useState('home');
	const [fetchedUser, setUser] = useState(null);
	const [popout, setPopout] = useState(<ScreenSpinner size='large' />);
	/*
	useEffect(() => {
		bridge.subscribe(({ detail: { type, data }}) => {
			if (type === 'VKWebAppUpdateConfig') {
				const schemeAttribute = document.createAttribute('scheme');
				schemeAttribute.value = data.scheme ? data.scheme : 'client_light';
				document.body.attributes.setNamedItem(schemeAttribute);
			}
		});
		async function fetchData() {
			const user = await bridge.send('VKWebAppGetUserInfo');
			setUser(user);
			setPopout(null);
		}
		fetchData();
	}, []);
	*/

	const go = e => {
		setActivePanel(e.currentTarget.dataset.to);
	};

	/*return (
		<View activePanel={activePanel} popout={popout}>
			<Home id='home' fetchedUser={fetchedUser} go={go} />
			<Persik id='persik' go={go} />
		</View>
	);*/
	return (
		<div>
			<Index memory={memory}/>
		</div>
	);
}

export default App;

